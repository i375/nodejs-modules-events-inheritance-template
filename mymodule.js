const EventEmmiter = require("events")
const utils = require("util")

function MyClass(){
    EventEmmiter.call(this)
    
    this.emitSomeEvent = function(){
        console.log("emmmiting event")
        this.emit("myEvent", 123)    
    }
    
    this.boomBox = function()
    {
        console.log("boom box")
    }
}

utils.inherits(MyClass, EventEmmiter)


exports.MyClass = MyClass
